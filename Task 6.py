import numpy as np
import matplotlib.pyplot as plt

fs = 20
t = np.linspace(0, 2.45, int(2.45 * fs))

# Signal 1: Basic Sine Wave
signal1 = np.sin(2 * np.pi * 1 * t)

# Signal 2: Noise Signal
signal2 = np.random.randn(len(t))

# Signal 3: Signal 1 with added noise
signal3 = signal1 + 10 * signal2

plt.figure()

# Subplot for Signal 1
plt.subplot(3, 1, 1)
plt.stem(t, signal1, basefmt=" ")
plt.title('Signal 1: Basic Sine Wave')

# Subplot for Signal 2
plt.subplot(3, 1, 2)
plt.stem(t, signal2, basefmt=" ")
plt.title('Signal 2: Noise Signal')

# Subplot for Signal 3
plt.subplot(3, 1, 3)
plt.stem(t, signal3, basefmt=" ")
plt.title('Signal 3: Signal 1 with Noise')

plt.tight_layout()
plt.show()