import numpy as np
import matplotlib.pyplot as plt
# Example signal generation
t = np.linspace(0, 1, 100, endpoint=False)
signal_1 = np.sin(2 * np.pi * 5 * t)  # e.g., 5 Hz sine wave
signal_3 = np.cos(2 * np.pi * 3 * t)  # e.g., 3 Hz cosine wave
signal_4 = np.sin(2 * np.pi * 10 * t) # e.g., 10 Hz sine wave

fig, axs = plt.subplots(3, 1, figsize=(10, 8))

axs[0].stem(t, signal_1)
axs[1].stem(t, signal_3)
axs[2].stem(t, signal_4)

for ax in axs:
    ax.set_xlabel('Time [s]')
    ax.set_ylabel('Amplitude')
plt.tight_layout()
plt.show()

# Observations:
# Signal 1 shows a standard 5 Hz sine wave pattern...
# Signal 3 appears to be a 3 Hz cosine wave with...
# Signal 4 demonstrates a higher frequency sine wave at 10 Hz...