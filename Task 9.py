import matplotlib.pyplot as plt
import numpy as np

# Example signal generation
t = np.linspace(0, 1, 100, endpoint=False)
signal_2 = np.cos(2 * np.pi * 4 * t)  # Example: 4 Hz cosine wave
signal_3 = np.sin(2 * np.pi * 3 * t)  # Example: 3 Hz sine wave
signal_5 = np.cos(2 * np.pi * 7 * t)  # Example: 7 Hz cosine wave

fig, axs = plt.subplots(3, 1, figsize=(10, 8))

# Plot each signal on a separate subplot
axs[0].stem(t, signal_2)
axs[0].set_title('Signal 2')
axs[1].stem(t, signal_3)
axs[1].set_title('Signal 3')
axs[2].stem(t, signal_5)
axs[2].set_title('Signal 5')

for ax in axs:
    ax.set_xlabel('Time [s]')
    ax.set_ylabel('Amplitude')
plt.tight_layout()
plt.show()

# Observations:
# Signal 2 shows a cosine wave pattern with a frequency of 4 Hz, suggesting...
# Signal 3 is a 3 Hz sine wave that...
# Signal 5 presents a higher frequency cosine wave at 7 Hz, characterized by...