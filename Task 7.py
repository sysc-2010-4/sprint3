from scipy import signal
help(signal.lfilter)

import numpy as np
from scipy import signal
import matplotlib.pyplot as plt

# Define the filter coefficients (example)
b = [1, -1]  # Example coefficients for a high-pass filter
a = [1, 0.5] # Example coefficients for a low-pass filter

# Generate a sample signal (example)
t = np.linspace(0, 1, 100, endpoint=False)
signal_4 = np.sin(2 * np.pi * 5 * t)  # 5 Hz sine wave

# Apply the filter
filtered_signal = signal.lfilter(b, a, signal_4)

plt.plot(t, signal_4, label='Original Signal')
plt.plot(t, filtered_signal, label='Filtered Signal')
plt.xlabel('Time [s]')
plt.ylabel('Amplitude')
plt.legend()
plt.show()